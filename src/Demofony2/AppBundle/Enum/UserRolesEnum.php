<?php
namespace Demofony2\AppBundle\Enum;

class UserRolesEnum extends Enum
{
    const ROLE_ADMIN       = 'ADMIN';
    const ROLE_USER       = 'USER';
    const ROLE_EDITOR       = 'EDITOR';
}
