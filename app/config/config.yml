imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

parameters:
    router.request_context.host: %app_domain_name%

framework:
    #esi:             ~
    translator:      { fallback: "%locale%" }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
    templating:
        engines: ['twig']
        #assets_version: SomeVersionScheme
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # handler_id set to null will use default session handler from php.ini
        handler_id:  ~
        name: demofony2
    fragments:       ~
    http_method_override: true

# Sensio Framework Extra
sensio_framework_extra:
    view:
        annotations: false

# Twig
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    form:
        resources:
            - "bootstrap_3_layout.html.twig"
            - "SonataCoreBundle:Form:datepicker.html.twig"
    globals:
        google_analytics_enabled: %google_analytics_enabled%
        google_analytics_account: "%google_analytics_account%"

# Assetic
assetic:
    debug:          "%kernel.debug%"
    use_controller: false
    bundles:        [ ]

# Doctrine
doctrine:
    dbal:
        driver:   "%database_driver%"
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8
    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        auto_mapping: true
        naming_strategy: doctrine.orm.naming_strategy.underscore
        filters:
            softdeleteable:
                class: Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter
                enabled: true

# Swiftmailer
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    spool:     { type: memory }

# Vich Uploader
vich_uploader:
    db_driver: orm
    mappings:
        category_image:
            uri_prefix: /uploads/images/category
            upload_destination: %kernel.root_dir%/../web/uploads/images/category
            inject_on_load: true
            delete_on_remove: true
            delete_on_update: true
            namer: vich_uploader.namer_origname
        user_profile_image:
            uri_prefix: /uploads/images/user
            upload_destination: %kernel.root_dir%/../web/uploads/images/user
            inject_on_load: true
            delete_on_remove: true
            delete_on_update: true
            namer: vich_uploader.namer_origname
        participation_document:
            uri_prefix: /uploads/documents
            upload_destination: %kernel.root_dir%/../web/uploads/documents
            inject_on_load: true
            delete_on_remove: true
            delete_on_update: true
            namer: vich_uploader.namer_origname
        participation_image:
            uri_prefix: /uploads/images/participation
            upload_destination: %kernel.root_dir%/../web/uploads/images/participation
            inject_on_load: true
            delete_on_remove: true
            delete_on_update: true
            namer: vich_uploader.namer_origname

# Liip Imagine
liip_imagine:
    resolvers:
       default:
          web_path: ~
    filter_sets:
        cache: ~
        small:
            quality: 85
            filters:
                thumbnail: { size: [ 320, null ], mode: outbound }
        big:
            quality: 75
            filters:
                thumbnail: { size: [ 800, null ], mode: outbound }
        400xY:
            quality: 80
            filters:
                thumbnail: { size: [ 400, null ], mode: outbound }
        80x80:
            quality: 90
            filters:
                thumbnail: { size: [ 80, 80 ], mode: outbound }

# JMS i18n Routing
jms_i18n_routing:
    default_locale: "%locale%"
    locales: [ ca, es, en ]
    strategy: prefix

# JMS Translations
jms_translation:
    configs:
        app:
            dirs: [ %kernel.root_dir%, %kernel.root_dir%/../src ]
            output_dir: %kernel.root_dir%/Resources/translations
            excluded_names: [ "*TestCase.php", "*Test.php" ]
            excluded_dirs: [ cache, data, logs, Application ]
            extractors: [ jms_i18n_routing ]

# Doctrine Extensions
stof_doctrine_extensions:
    default_locale: %locale%
    translation_fallback: true
    orm:
        default:
            translatable:   false
            softdeleteable: true
            timestampable:  true
            sluggable:      true
            tree:           true

# Sonata Block
sonata_block:
    default_contexts: [ admin ]
    blocks:
#        sonata.user.block.menu:
#        sonata.user.block.account:
        sonata.block.service.text:
        sonata.admin.block.admin_list:

# Sonata Admin
sonata_admin:
    title: 'Demofony2'
    title_logo: /bundles/espaideltapage/images/logo.png
    options:
        html5_validate: true
        confirm_exit:   true
        use_select2:    true
        pager_links:    10
    persist_filters: true
#    templates:
#        layout:     ::Admin/custom_layout.html.twig
#        list_block: ::Admin/block_admin_list.html.twig
#        add_block:  ::Admin/add_block.html.twig

# FOS User
fos_user:
    db_driver: orm
    firewall_name: main
    user_class: Demofony2\UserBundle\Entity\User
    registration:
        form:
            type:  demofony2_user_registration

# FOS Rest
fos_rest:
    routing_loader:
        default_format: json
        include_format: false
    param_fetcher_listener: true
    body_listener: true
    format_listener: true
    view:
        view_response_listener: 'force'
    exception:
        messages:
              'Exception': true  #allows all exceptions to pass message to client in production

# KNP Paginator
knp_paginator:
    page_range: 10
    template:
        pagination: KnpPaginatorBundle:Pagination:twitter_bootstrap_v3_pagination.html.twig

# Nelmio API Doc
nelmio_api_doc: ~
